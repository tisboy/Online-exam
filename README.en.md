<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">Element Team</h1>
<h4 align="center">Online Knowledge Contest Website - Based on RuoYi</h4>
<p align="center">
	<a href="https://gitee.com/mnsfhxy/element-knowledge-contest/tree/master">Version 1.0</a>
</p>

## Ruoyi profile



Ruoyi is a full set of open source rapid development platform, without reservation for individuals and enterprises to use free.



* Front end uses Vue, Element UI.

* The backend uses Spring Boot, Spring Security, Redis & Jwt.

* Authorization authentication using Jwt, support multi-terminal authentication system.

* Support loading dynamic permissions menu, multiple easy permissions control.

* Efficient development, the use of code generator can be a key to generate the front and back end code.



## Project Introduction

The team members are all ordinary undergraduates from a university. This project is used for the final software engineering practice training



## Function introduction - student end



1. Students can participate in the online contest by logging in with their account and password. If the administrator has not added a student account, students can register by themselves.

2. After entering the contest system, students have two functional menus: online contest and my score.

3. Online contest: Online contest displays the contest information created by the current administrator, including the contest name, contest type, contest date limit, contest duration limit, total score, and pass line. Students click the corresponding button to enter the preparation competition.

4. Prepare contest: The basic information of the contest is displayed on the prepare contest page. Click "Start Contest" to enter the contest interface.

5. Start contest: After the contest begins, the remaining time and paper submission will be displayed at the top of the page, the answer sheet will be displayed on the left side of the page, and questions and options will be displayed in the center of the page.

6. Results of competition: after students hand in their papers, the page displays the results of competition, from which students can learn the correct answers, selected answers, test scores and other information.

7. My Score: Students can view their scores and corresponding details through my score interface.



## Function introduction - Administrator side

1. The administrator has two main functions: test management and system setting.

2. Question bank management: The administrator can add question bank and delete the created question bank through question bank management.

3. Test question management: the administrator can add test questions through test question management, the types of test questions are: single choice, multiple choice, judgment, and can set the answer analysis of the topic; To facilitate adding and modifying questions in batches, the system provides import and export functions. Users can add and upload questions in batches using the provided Excel template, and export existing questions in the system to a local computer.

4. Contest management: The administrator can view the details of the corresponding contest through the contest management, and can add, modify, and delete the existing contest.

5. System configuration: The administrator answers questions through system configuration.

6. College Management: Administrators can maintain college information through college management.

7. Role management: Administrators can view role information through role management.

8. User management: Administrators can add or delete users through user management.


# # demo figure
no

## Prototype design drawing
<a href="https://mastergo.com/file/63784303723973?page_id=83:5062&source=link_share">ElementTeam Prototype design drawing</a>