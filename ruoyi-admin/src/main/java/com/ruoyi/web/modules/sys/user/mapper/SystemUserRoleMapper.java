package com.ruoyi.web.modules.sys.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.web.modules.sys.user.entity.SysUserRole;
import org.apache.ibatis.annotations.Mapper;

/**
* 用户角色Mapper
*/
@Mapper
public interface SystemUserRoleMapper extends BaseMapper<SysUserRole> {

}
