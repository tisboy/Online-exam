package com.ruoyi.web.modules.exam.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.web.modules.exam.entity.ExamDepart;
import org.apache.ibatis.annotations.Mapper;

/**
* 考试部门Mapper
*/
@Mapper
public interface ExamDepartMapper extends BaseMapper<ExamDepart> {

}
