package com.ruoyi.web.modules.qu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.web.modules.qu.entity.QuAnswer;
import org.apache.ibatis.annotations.Mapper;


/**
* 候选答案Mapper
*/
@Mapper
public interface QuAnswerMapper extends BaseMapper<QuAnswer> {

}
