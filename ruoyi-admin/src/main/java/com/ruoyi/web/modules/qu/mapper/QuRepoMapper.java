package com.ruoyi.web.modules.qu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.web.modules.qu.entity.QuRepo;
import org.apache.ibatis.annotations.Mapper;

/**
* 试题题库Mapper
*/
@Mapper
public interface QuRepoMapper extends BaseMapper<QuRepo> {

}
