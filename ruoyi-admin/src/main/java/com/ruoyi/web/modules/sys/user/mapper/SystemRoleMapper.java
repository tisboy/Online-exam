package com.ruoyi.web.modules.sys.user.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.web.modules.sys.user.entity.SysRole;
import org.apache.ibatis.annotations.Mapper;


/**
* 角色Mapper
*/
@Mapper
public interface SystemRoleMapper extends BaseMapper<SysRole> {

}
